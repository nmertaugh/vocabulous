#  Libraries
require 'pry'
require_relative 'word'
require_relative 'word_set'
require 'yaml'

# Constants
YAML_PATH = "#{File.expand_path File.dirname(__FILE__)}/words.yml"

def begin_quiz
  puts "There are #{words.count} words to play with today."
  puts 'Which sort method would you like, alphabetical, shuffle, or by letter?'
  print '(A | S | a-z) '
  sort_mode_user_input = read

  sort_mode =
    case sort_mode_user_input
    when 'A'
      :alphabetical
    when 'S'
      :shuffle
    when /[a-z]/
      :by_letter
    end
  word_set = WordSet.new(words, sort_mode, sort_mode_user_input)
  puts "#{sort_mode.to_s.gsub('_', ' ').capitalize} it is! We have #{word_set.count} words for you."

  word_set.words.each_with_index do |word, index|
    puts "\n\n#{horizontal_rule}"
    puts "|  #{colorize(word.body)} "
    print Array.new(word.body.length + 6) { '-' }.join
    print "\n|  (enter to reveal the definition, any character to exit)" if index == 0

    if read.empty?
      word.parts_of_speech_and_definitions.each_pair do |part_of_speech, definitions|
        puts "| \n|  #{part_of_speech}:"

        definitions.each_with_index do |definition, outer_index|
          wrapped_lines(definition).each_with_index do |line, inner_index|
            puts '|' if (definitions.count > 1 && inner_index == 0 && outer_index != 0)
            prefix = (definitions.count > 1 && inner_index == 0) ? "#{outer_index + 1}: " : ''
            spacer = (definitions.count > 1 && inner_index > 0) ? '   ' : ''
            puts "|   #{prefix}#{spacer}#{colorize(line, :yellow)}"
          end
        end
      end

      puts '|'
      puts horizontal_rule
    else
      exit
    end
  end

  puts 'That\'s all the words we have for you! Thank you for playing!'
end

def colorize(string, color = :green)
  color_code =
    case color
    when :blue
      34
    when :green
      32
    when :light_blue
      36
    when :yellow
      33
    end

  "\e[#{color_code}m#{string}\e[0m"
end

def horizontal_rule
  '----------------------------------------------------------------'
end

def read
  gets.chomp.strip
end

def words
  @words ||= begin
    YAML.load_file(YAML_PATH).map do |word, parts_of_speech_and_definitions|
      Word.new word, parts_of_speech_and_definitions
    end
  end
end

def wrapped_lines(string, width = 55)
  string.gsub(/(.{1,#{width}})(\s+|\Z)/, "\\1\n").split "\n"
end

begin_quiz
