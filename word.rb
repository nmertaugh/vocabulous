class Word
  # Attributes
  attr_reader :body, :parts_of_speech_and_definitions

  def initialize(body, parts_of_speech_and_definitions)
    @body = body
    @parts_of_speech_and_definitions = parts_of_speech_and_definitions
  end
end
