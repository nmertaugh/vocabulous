class WordSet < SimpleDelegator
  # Attributes
  attr_reader :words

  def initialize(words, sort_mode = :shuffle, start_letter = nil)
    @sort_mode = sort_mode
    @start_letter = start_letter.strip
    @words = sort_words_by_sort_mode(words).to_enum

    super @words
  end

  private

  def sort_words_alphabetically(words)
    words.sort_by { |word| word.body.downcase }
  end

  def sort_words_by_sort_mode(words)
    case @sort_mode
    when :alphabetical
      sort_words_alphabetically words
    when :by_letter
      scoped_words = words.select { |word| word.body[0].downcase == @start_letter.downcase }
      sort_words_alphabetically scoped_words
    when :shuffle
      words.shuffle
    end
  end
end
