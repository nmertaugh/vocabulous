class DictionaryHelper
  def self.lookup(words)
    words_and_definitions = Array(words).each_with_object({}) do |word, hash|
      hash[word] = `dict -d gcide #{word}`
    end

    words_and_definitions.each_pair do |word, definition|
      puts "\n\n#{word}:"
      puts "  #{definition}"
    end

    nil
  end
end
